#
# MNIST estimation derived from TensorFlow tutorial model
# Training the model (applied the following patch) precedes running this estimation script
#
# diff --git a/tensorflow/examples/tutorials/layers/cnn_mnist.py 
# @@ 31 @@ def cnn_model_fn(features, labels, mode):
# -      input_layer = tf.reshape(features["x"], [-1, 28, 28, 1])
# +      input_layer = tf.reshape(features["x"], [-1, 28, 28, 1], name="input")
# @@ 91 @@ def cnn_model_fn(features, labels, mode):
# -      "classes": tf.argmax(input=logits, axis=1),
# +      "classes": tf.argmax(input=logits, axis=1, name="output"),
# @@ 128 @@ def main(unused_argv):
# -      model_fn=cnn_model_fn, model_dir="/tmp/mnist_convnet_model")
# +      model_fn=cnn_model_fn, model_dir="./mnist_convnet_model")
#
# For practical use, you must export the trained model as a zipped file
# - The zipped file is called "frozen model"
# Freezing the trained model
# - freeze_graph --input_binary=false --input_graph=./mnist_convnet_model/graph.pbtxt --input_checkpoint=./mnist_convnet_model/model.ckpt-20000 --output_graph=frozen_graph.pb --output_node_names=output,softmax_tensor
# Optimizing for inference removes parts of the graph that are only needed for training
# - optimize_for_inference --input=./frozen_graph.pb --output=./optimized_graph.pb --input_names=input --output_names=output,softmax_tensor
#

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import tensorflow as tf

tf.logging.set_verbosity(tf.logging.INFO)

def load_graph(my_path):
  # Load the pb file and parse it to retrieve the
  # unserialized graph_def
  with tf.gfile.GFile(my_path, "rb") as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())

  # Now import the graph_def to our default graph
  with tf.Graph().as_default() as graph:
    tf.import_graph_def(
      graph_def,
      input_map=None,
      return_elements=None,
      # If you put name=None here instead of ''
      # it will relabel all your ops as "import/original_name"
      name='',
      producer_op_list=None
    )
  return graph

tf.reset_default_graph()

filename = "./optimized_graph.pb"

graph = load_graph(filename)

# Check the model contains tensors to be used for prediction
input = graph.get_tensor_by_name("input:0")
output = graph.get_tensor_by_name("output:0")
softmax = graph.get_tensor_by_name("softmax_tensor:0")

with tf.Session(graph=graph) as sess:
  # Now we don't need to initialize any variables because
  # there aren't any, only constants

  # We set batch_size=100 previously, so we must input 100 images
  # even though we want to predict for a few images
  # examples.npy can be found on https://github.com/tensorflow/models/blob/master/official/mnist/examples.npy
  arr = np.load('examples.npy')
  arr = np.reshape(arr, (-1, 28, 28, 1))
  arr = np.append(arr, np.zeros((98, 28, 28, 1)), axis=0)
  arr = np.ones((100, 28, 28, 1)) - arr

  res = sess.run([softmax, output], feed_dict={input: arr})
  
  # If you use examples.npy, the output seems like:
  # output: [5 3 8 8 8 8 8 (dummy) ...]
  print ("output: ", res[1])
